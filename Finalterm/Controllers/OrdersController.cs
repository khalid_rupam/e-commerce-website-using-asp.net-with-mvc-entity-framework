﻿using Finalterm.Models;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Finalterm.Controllers
{
    public class OrdersController : SecureController
    {
        OrderRepository repo = new OrderRepository(new Models.FinalProjectEntities());
        CartsRepository repoCart = new CartsRepository(new Models.FinalProjectEntities());
        CustomerRepository customrepo = new CustomerRepository(new Models.FinalProjectEntities());
        // GET: Orders
        public ActionResult Index()
        {
            Customer c = customrepo.GetById(Session["Login"].ToString());

            Order o = repo.GetAll().ToList().OrderBy(p=>p.time).Where(p=>p.C_Id== Session["Login"].ToString()).FirstOrDefault();


            SmtpClient clientDetais = new SmtpClient();
            clientDetais.Port = 587;
            clientDetais.Host = "smtp.gmail.com";
            clientDetais.EnableSsl = true;
            clientDetais.DeliveryMethod = SmtpDeliveryMethod.Network;
            clientDetais.UseDefaultCredentials = false;
            clientDetais.Credentials = new NetworkCredential("librarymanagementSystemAIUB@gmail.com", "khalidhasan.khr");

            MailMessage maildetails = new MailMessage();
            maildetails.From = new MailAddress("librarymanagementSystemAIUB@gmail.com");
            maildetails.To.Add(c.Email);
            maildetails.Subject = "Order Has been Confirm";
            maildetails.Body = "Thank you for shoping with BuyHere. Your Order id"+o.ID+" Permanet address: "+o.Address+"Payment address: "+o.Address+"Total Cost"+o.Payment.Amount;
            clientDetais.Send(maildetails);
            return View(o);
        }
        public ActionResult Create()
        {
            string cid = Session["Login"].ToString();
            List<Cart> tempCart = (List<Cart>)Session["cart"];
            foreach (var item in tempCart)
            {
                Order temp = new Order();
                temp.C_Id = item.C_ID;
                temp.P_ID = item.P_Id;
                temp.Quantity = item.Quantity;
                temp.Status = 0;
                temp.Pay_id = 1;
                temp.time = DateTime.Now.ToShortDateString();
                //////////////////////////////////////////////////////////////////////////////////////////////
                ///
                temp.Address = item.Customer.Address;
                repo.Insert(temp);
                repoCart.Delete(repoCart.GetById(item.Id.ToString()));
            }
            Session["cart"] = null;
            Customer customer = new Customer();
            customer = customrepo.GetById(cid);
            int total = Convert.ToInt32(Session["total"]);
            if(total>=500)
            {
                customer.Points += total / 10;
                customrepo.Update(customer);
            }
            Session["total"] = null;
            return RedirectToAction("Index");
        }
    }
}