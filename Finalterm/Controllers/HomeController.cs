﻿using Finalterm.Interface;
using Finalterm.Models;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Finalterm.Controllers
{
    public class HomeController : Controller
    {
        IRepository<Product> prepo = new ProductRepostory(new FinalProjectEntities());
        IRepository<frentite> frrepo = new FrontRepository(new FinalProjectEntities());
        IRepository<Customer> crrepo = new CustomerRepository(new FinalProjectEntities());
        IRepository<Order> orrepo = new OrderRepository(new FinalProjectEntities());
        IRepository<Login> lrepo = new LoginRepository(new FinalProjectEntities());
        IRepository<Product> papo = new ProductRepostory(new FinalProjectEntities());
        IRepository<Catagory> catra = new CatagoryRepository(new FinalProjectEntities());
        IRepository<Comment> capo = new CommentRepository(new FinalProjectEntities());

        public ActionResult start()
        {
            Session["Login"] = 0;
            Session["Id"] = 0;
            return RedirectToAction("Index");
        }
        [HttpGet]
        public ActionResult Index()
        {
            Product p = new Product();
            List<frentite> lf= frrepo.GetAll().ToList().OrderBy(p1=>p1.Id).ToList();
            int count = 0;
            foreach (var item in lf)
            {
                if(count==0)
                {
                    ViewBag.zero = item.productId;
                    ViewBag.one = item.time.ToString();
                }
                else if(count==1)
                {
                    ViewBag.three = item.productId;
                    ViewBag.two = item.time.ToString();
                }
                else if (count == 2)
                {
                    ViewBag.four = item.productId;
                    ViewBag.five = item.time.ToString();
                }
                count += 1;
            }
            List<Catagory> cat = catra.GetAll().ToList();
            ViewBag.Cata = cat;
            ViewBag.Pata = papo.GetAll().ToList();
            return View(papo.GetAll().ToList());
        }
        [HttpPost]
        public ActionResult Index(string name)
        {
            return Content(name);
        }
        [HttpGet]
        public ActionResult Details(int id)
        {
            Session["pid"] = id;
            List<Comment> cm = new List<Comment>();

            List<Comment> lc = capo.GetAll().ToList();
            foreach (var item in lc)
            {
                if (item.P_Id == id)
                {
                    Comment cmd = new Comment();
                    cmd.ID = item.ID;
                    cmd.Product = item.Product;
                    cmd.P_Id = item.P_Id;
                    cmd.rating = item.rating;
                    cmd.time = item.time;
                    cmd.Comment1 = item.Comment1;
                    cmd.Annonyms = item.Annonyms;
                    cmd.Customer = item.Customer;
                    cm.Add(cmd);
                }

            }
            ViewBag.cus = cm;
            double count = 0.0;
            double rat = 0.0;
            foreach (var item in lc)
            {
                if (item.P_Id == id)
                {
                    count += item.rating;
                    rat += 1;
                }
            }
            double d = count / rat;
            ViewBag.avgrating = d;
            return View(papo.GetAll());


        }
        [HttpPost]
        public ActionResult Details()
        {
            Comment c = new Comment();
            return View(papo.GetAll());
        }
        public ActionResult Logout()
        {
            Login l = lrepo.GetById(Session["Login"].ToString());
            LoginRepository lr = new LoginRepository(new FinalProjectEntities());
            l.Online = 0;
            lr.update(l);
            Session["Login"] = 0;
            Session["Id"] = 0;
            return RedirectToAction("Index");

        }
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(Login l)
        {
            Login l1= lrepo.GetById(l.ID);
            if (l1==null)
            {
                return View("Login");
            }
            else
            {
                if (l1.ID==l.ID&&l1.Password==l.Password)
                {
                    if(l1.Status==0&&l1.Type==0)
                    {
                        Session["Login"] = l1.ID;
                        Session["Id"] = "1";
                        return RedirectToAction("Index");
                    }
                    else if(l1.Status == 0 && l1.Type == 1)
                    {
                        Session["Login"] = l1.ID;
                        Session["Id"] = "1";
                        return RedirectToAction("Admin");
                    }
                }
                else
                {
                    return View("Index");
                }
                return View("Index");
            }

        }
        public ActionResult ShopNow(string name)
        {
            return Content(name);
        }
        public ActionResult Admin()
        {
            AdminDashboard ad = new AdminDashboard();
            ad.NoOfOrders = orrepo.GetAll().ToList().Count();


            int i = lrepo.GetAll().Where(p => p.Online == 1).Count();

            ad.Online = i;

            List<Order> ol = orrepo.GetAll().ToList();
            List<Order> fl = new List<Order>();
            ad.NoOfSales = 0;
            ad.Jan = 0;
            ad.feb = 0;
            ad.march = 0;
            ad.april = 0;
            ad.may = 0;
            ad.jun = 0;
            ad.july = 0;
            ad.Aug = 0;
            ad.Sept = 0;
            ad.Oct = 0;
            ad.Nov = 0;
            ad.Dec = 0;
            string s= DateTime.Now.ToString();
            foreach (var item in ol)
            {
                if(item.Status==1)
                {
                    ad.NoOfSales += 1;
                }
                else
                {
                    fl.Add(item);
                }
            }
            foreach (var item in ol)
            {
                if(item.time.ToString().Substring(0, 2)=="1/")
                {
                    ad.Jan += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "2/")
                {
                    ad.feb += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "3/")
                {
                    ad.march += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "4/")
                {
                    ad.april += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "5/")
                {
                    ad.may += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "6/")
                {
                    ad.jun += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "7/")
                {
                    ad.july += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "8/")
                {
                    ad.Aug += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "9/")
                {
                    ad.Sept += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "10")
                {
                    ad.Oct += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "11")
                {
                    ad.Nov += 1;
                }
                else if (item.time.ToString().Substring(0, 2) == "12")
                {
                    ad.Dec += 1;
                }
            }
            ad.NoOfCustomer = crrepo.GetAll().ToList().Count();
            ad.orders = fl;
            return View(ad);
        }
        public ActionResult Discount()
        {
            return Content("I am here");    
        }
        public ActionResult AddComment(int quantity, string anno, string comment)
        {
            Comment c = new Comment();
            c.Annonyms = int.Parse(anno);
            c.P_Id = int.Parse(Session["pid"].ToString());
            c.time = DateTime.Now.ToString();
            c.rating = quantity;
            c.C_Id = Session["Id"].ToString();
            c.Comment1 = comment;
            capo.Insert(c);
            return RedirectToAction("Index");
        }
        public ActionResult ViewComment(Comment com)
        {
            return View();

        }
    }
}