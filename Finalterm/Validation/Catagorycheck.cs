﻿using Finalterm.Interface;
using Finalterm.Models;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Finalterm.Validation
{
    public class Catagorycheck:ValidationAttribute
    {
        IRepository<Catagory> crepo = new CatagoryRepository(new FinalProjectEntities());
        public override bool IsValid(object value)
        {
            int s = (int)value;
            Catagory c = crepo.GetAll().Where(p => p.Id == s).FirstOrDefault();
            if (c == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}