﻿using Finalterm.Interface;
using Finalterm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finalterm.Repository
{
    public class DiscountRepository:Repository<Discount>
    {
        
        public DiscountRepository(FinalProjectEntities fr):base(fr)
        {

        }
    }
}