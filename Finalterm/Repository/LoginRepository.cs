﻿using Finalterm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finalterm.Repository
{
    public class LoginRepository:Repository<Login>
    {

        FinalProjectEntities f = new FinalProjectEntities();
        public LoginRepository(FinalProjectEntities fr):base(fr)
        {
        }
        public void update(Login o)
        {
            Login o1 = f.Logins.Find(o.ID);
            o1.ID = o.ID;
            o1.Online = o.Online;
            o1.Password = o.Password;
            o1.Status = o.Status;
            o1.Type = o.Type;
            f.SaveChanges();
        }
    }
}