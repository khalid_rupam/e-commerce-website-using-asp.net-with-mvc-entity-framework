﻿using Finalterm.Interface;
using Finalterm.Models;
using Finalterm.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Finalterm.Controllers
{
    public class RupamOrderController : SecureController
    {
        IRepository<Order> Orrepo = new OrderRepository(new FinalProjectEntities());
        IRepository<Customer> crrepo = new CustomerRepository(new FinalProjectEntities());
        IRepository<Admin> adrepo = new AdminRepository(new FinalProjectEntities());
        IRepository<Login> lrepo = new LoginRepository(new FinalProjectEntities());
        IRepository<Catagory> crepo = new CatagoryRepository(new FinalProjectEntities());
        IRepository<Discount> drepo = new DiscountRepository(new FinalProjectEntities());
        IRepository<frentite> frepo = new FrontRepository(new FinalProjectEntities());
        IRepository<Product> prepo = new ProductRepostory(new FinalProjectEntities());
        IRepository<Comment> comrepo = new CommentRepository(new FinalProjectEntities());
        IRepository<Message> mrepo = new MessageRepository(new FinalProjectEntities());
        [HttpGet]
        public ActionResult NoOfOrder()
        {
            return View(Orrepo.GetAll().ToList());
        }
        [HttpPost]
        public ActionResult NoOfOrder(string name,string Select)
        {
            Session["done"] = 0;
            string temp;
            if (name.Equals(""))
            {
                return RedirectToAction("NoOfOrder");
            }
            List<Order> lo= Orrepo.GetAll().ToList();
            List<Order> lso = new List<Order>();
            Order o = new Order();
            foreach (var item in lo)
            {
                if(Select.Equals("Id"))
                {
                    if (name.Equals(item.ID.ToString()))
                    {
                        o.ID = item.ID;
                        o.Pay_id = item.Pay_id;
                        o.P_ID = item.P_ID;
                        o.Quantity = item.Quantity;
                        o.Sales = item.Sales;
                        o.Status = item.Status;
                        o.time = item.time;
                        o.C_Id = item.C_Id;
                        o.Customer = item.Customer;
                        o.Payment = item.Payment;
                        o.Product = item.Product;
                        Session["done"] = 1;
                        return View("OneOrder", o);
                    }
                }
                else if(Select.Equals("Name"))
                {
                    if (name.Equals(item.Customer.Name))
                    {
                        o.ID = item.ID;
                        o.Pay_id = item.Pay_id;
                        o.P_ID = item.P_ID;
                        o.Quantity = item.Quantity;
                        o.Sales = item.Sales;
                        o.Status = item.Status;
                        o.time = item.time;
                        o.C_Id = item.C_Id;
                        o.Customer = item.Customer;
                        o.Payment = item.Payment;
                        o.Product = item.Product;
                        Session["done"] = 1;
                        lso.Add(item);
                    }
                }
                else if (Select.Equals("Status"))
                {
                    if(name.ToUpper().Equals("PENDING"))
                    {
                        temp = "0";
                    }
                    else
                    {
                        temp = "1";
                    }
                    if (temp.Equals(item.Status.ToString()))
                    {
                        o.ID = item.ID;
                        o.Pay_id = item.Pay_id;
                        o.P_ID = item.P_ID;
                        o.Quantity = item.Quantity;
                        o.Sales = item.Sales;
                        o.Status = item.Status;
                        o.time = item.time;
                        o.C_Id = item.C_Id;
                        o.Customer = item.Customer;
                        o.Payment = item.Payment;
                        o.Product = item.Product;
                        lso.Add(item);
                        Session["done"] = 1;
                    }
                }
                else if (Select.Equals("Amount"))
                {
                    if (name.Equals(item.Payment.Amount.ToString()))
                    {
                        o.ID = item.ID;
                        o.Pay_id = item.Pay_id;
                        o.P_ID = item.P_ID;
                        o.Quantity = item.Quantity;
                        o.Sales = item.Sales;
                        o.Status = item.Status;
                        o.time = item.time;
                        o.C_Id = item.C_Id;
                        o.Customer = item.Customer;
                        o.Payment = item.Payment;
                        o.Product = item.Product;
                        Session["done"] = 1;
                        lso.Add(item);
                    }
                }

            }
            return View(lso);
            
        }
        [HttpGet]
        public ActionResult Details(int id)
        {
            return View(Orrepo.GetById(id.ToString()));
        }
        [HttpPost]
        public ActionResult Details(Order o)
        {
            OrderRepository or = new OrderRepository(new FinalProjectEntities());
            o.time = DateTime.Now.ToString();
            or.update(o);
            return RedirectToAction("NoOfOrder");
        }
        public ActionResult Print(int id)
        {
            return View(Orrepo.GetById(id.ToString()));
        }
        public ActionResult CusDetails()
        {
            return View(crrepo.GetAll().ToList());
        }
        [HttpGet]
        public ActionResult cdetails(string id)
        {
            return View(crrepo.GetById(id));
        }
        [HttpPost]
        public ActionResult cdetails(Customer c, int Status)
        {
            //FinalProjectEntities f = new FinalProjectEntities();
            CustomerRepository cr = new CustomerRepository(new FinalProjectEntities());
            cr.update(c, Status);
            //Customer c1 = f.Customers.Find(c.Id);
            //Login l1 = f.Logins.Find(c.Id);
            //c1.Id = c.Id;
            //c1.Name = c.Name;
            //c1.PhoneNumber = c.PhoneNumber;
            //c1.Email = c.Email;
            //c1.Address = c.Address;
            //l1.Status = Status;
            //c1.Points = c1.Points;
            //try
            //{
            //    // Your code...
            //    // Could also be before try if you know the exception occurs in SaveChanges

            //    f.SaveChanges();
            //    return RedirectToAction("Memory");
            //}
            //catch (DbEntityValidationException e)
            //{
            //    foreach (var eve in e.EntityValidationErrors)
            //    {
            //        //return Content("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:" +
            //        //    eve.Entry.Entity.GetType().Name + " " + eve.Entry.State);
            //        foreach (var ve in eve.ValidationErrors)
            //        {
            //            return Content("- Property: \"{0}\", Error: \"{1}\"" +
            //                ve.PropertyName + "," + ve.ErrorMessage);
            //        }
            //    }
                return RedirectToAction("CusDetails");
            
        }
        public ActionResult Delete(string id)
        {
            Customer c = crrepo.GetById(id);
            crrepo.Delete(c);
            return RedirectToAction("CusDetails");
        }
        [HttpGet]
        public ActionResult AddEmployee()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddEmployee(Admin a)
        {
            if(ModelState.IsValid)
            {
                Login l = new Login();
                l.ID = a.Id;
                Random r = new Random();
                string s = r.Next(999).ToString();
                l.Password = s;
                l.Status = 0;
                l.Type = 1;
                l.Online = 0;
                lrepo.Insert(l);

                adrepo.Insert(a);

                try
                {
                    SmtpClient clientDetais = new SmtpClient();
                    clientDetais.Port = 587;
                    clientDetais.Host = "smtp.gmail.com";
                    clientDetais.EnableSsl = true;
                    clientDetais.DeliveryMethod = SmtpDeliveryMethod.Network;
                    clientDetais.UseDefaultCredentials = false;
                    clientDetais.Credentials = new NetworkCredential("librarymanagementSystemAIUB@gmail.com", "khalidhasan.khr");

                    MailMessage maildetails = new MailMessage();
                    maildetails.From = new MailAddress("librarymanagementSystemAIUB@gmail.com");
                    maildetails.To.Add(a.Email);
                    maildetails.Subject = "Account Created";
                    maildetails.Body = "Welcome to BuyHere " + a.Name + "\n Your Employee Id:" + a.Id + "AND YOUR PASSWORD IS " + s;
                    clientDetais.Send(maildetails);
                    return RedirectToAction("Admin", "Home");
                }
                catch (Exception)
                {
                    return RedirectToAction("Admin", "Home");
                }
            }
            return View();
        }
        [HttpGet]
        public ActionResult AddCatagory()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddCatagory(Catagory c)
        {
            if(ModelState.IsValid)
            {
                crepo.Insert(c);
                return RedirectToAction("Admin","Home");
            }
            else
            {
                return View();
            }
        }
        [HttpGet]
        public ActionResult adddiscount()
        {
            return View();
        }
        [HttpPost]
        public ActionResult adddiscount(Discount d)
        {
           if(ModelState.IsValid)
            {
                drepo.Insert(d);
                return RedirectToAction("Admin","Home");
            }
           else
            {
                return View();
            }
        }
        [HttpGet]
        public ActionResult Banner()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Banner(frentite f, HttpPostedFileBase time)
        {
            if (ModelState.IsValid)
            {
                Random ran = new Random();
                string path = "-1";
                int random = ran.Next(99);
                if (time != null && time.ContentLength > 0)
                {
                    string extension = Path.GetExtension(time.FileName);
                    if (extension.ToLower().Equals(".jpg") || extension.ToLower().Equals(".png"))
                    {
                        //path = Path.Combine(Server.MapPath("~/Image/"), random + Path.GetFileName(time.FileName));
                        //time.SaveAs(path);
                        //path = "~/Image/" + random + Path.GetFileName(time.FileName);
                        //frentite fee = new frentite();
                        //fee.productId = f.productId;
                        //fee.time = path;
                        //frepo.Insert(fee);
                        //return RedirectToAction("Admin", "RupamOrder");
                        try
                        {
                            path = Path.Combine(Server.MapPath("~/Image/"), random + Path.GetFileName(time.FileName));
                            time.SaveAs(path);
                            path = "~/Image/" + random + Path.GetFileName(time.FileName);
                            frentite fee = new frentite();
                            fee.productId = f.productId;
                            fee.time = path;
                            frepo.Insert(fee);
                            return RedirectToAction("Admin", "Home");

                        }
                        catch (Exception)
                        {
                            return Content("Upload fail catch");
                        }
                    }
                    else
                    {
                        return Content("Uplad  else");
                    }

                }
                else
                {
                    return Content("unsupported file");
                }
            }
            else
            {
                return View();
            }

        }
        public ActionResult ViewCoupon()
        {
            return View(drepo.GetAll().ToList());
        }
        public ActionResult Circulate(string id)
        {
            List<Customer> cl=  crrepo.GetAll().ToList();
            Discount d = drepo.GetById(id);

            foreach (var item in cl)
            {
                SmtpClient clientDetais = new SmtpClient();
                clientDetais.Port = 587;
                clientDetais.Host = "smtp.gmail.com";
                clientDetais.EnableSsl = true;
                clientDetais.DeliveryMethod = SmtpDeliveryMethod.Network;
                clientDetais.UseDefaultCredentials = false;
                clientDetais.Credentials = new NetworkCredential("librarymanagementSystemAIUB@gmail.com", "khalidhasan.khr");

                MailMessage maildetails = new MailMessage();
                maildetails.From = new MailAddress("librarymanagementSystemAIUB@gmail.com");
                maildetails.To.Add(item.Email);
                maildetails.Subject = "GOOD NEWS";
                maildetails.Body = "GET "+d.Discount1+" Taka discount On any product. To Avail this off use "+d.Id+" Thank you";
                clientDetais.Send(maildetails);
            }
            return RedirectToAction("NoOfOrder");
            
        }
        public ActionResult DeleteCoupon(string id)
        {
            Discount d = drepo.GetById(id);
            if(d==null)
            {
                return Content("NULL");
            }
            else
            {
                drepo.Delete(d);
                return RedirectToAction("Admin", "Home");
            }
        }
        [HttpGet]
        public ActionResult AddProduct()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddProduct(Product p, HttpPostedFileBase image)
        {
            if (ModelState.IsValid)
            {
                Random ran = new Random();
                string path = "-1";
                int random = ran.Next(99);
                if (image != null && image.ContentLength > 0)
                {
                    string extension = Path.GetExtension(image.FileName);
                    if (extension.ToLower().Equals(".jpg") || extension.ToLower().Equals(".png"))
                    {
                        //path = Path.Combine(Server.MapPath("~/Image/"), random + Path.GetFileName(time.FileName));
                        //time.SaveAs(path);
                        //path = "~/Image/" + random + Path.GetFileName(time.FileName);
                        //frentite fee = new frentite();
                        //fee.productId = f.productId;
                        //fee.time = path;
                        //frepo.Insert(fee);
                        //return RedirectToAction("Admin", "RupamOrder");
                        try
                        {
                            path = Path.Combine(Server.MapPath("~/Image/"), random + Path.GetFileName(image.FileName));
                            image.SaveAs(path);
                            path = "~/Image/" + random + Path.GetFileName(image.FileName);
                            p.image = path;
                            prepo.Insert(p);

                            return RedirectToAction("Admin", "Home");

                        }
                        catch (Exception)
                        {
                            return Content("Upload fail catch");
                        }
                    }
                    else
                    {
                        return Content("Uplad  else");
                    }

                }
                else
                {
                    return Content("unsupported file");
                }

            }
            else
            {
                return View();
            }
        }
        public ActionResult ViewProduct()
        {
            return View(prepo.GetAll().ToList());
        }
        [HttpGet]
        public ActionResult ProductEdit(int id)
        {
            return View(prepo.GetById(id.ToString()));
        }
        [HttpPost]
        public ActionResult ProductEdit(Product p)
        {
            ProductRepostory pr = new ProductRepostory(new FinalProjectEntities());
            pr.update(p);
            return RedirectToAction("ViewProduct");
        }
        public ActionResult ProductDelete(int id)
        {
            prepo.Delete(prepo.GetById(id.ToString()));
            return RedirectToAction("ViewProduct");
        }
        public ActionResult Comments(int id)
        {
            return View(comrepo.GetAll().Where(p=>p.P_Id==id).ToList());
        }
        public ActionResult ComDelete(int id)
        {
            Comment c = comrepo.GetById(id.ToString());

            comrepo.Delete(c);
            return RedirectToAction("Admin","Home");
                
        }
        public ActionResult Message()
        {
            return View(mrepo.GetAll().ToList());
        }
        [HttpGet]
        public ActionResult Replied(string id)
        {
            Message m= mrepo.GetById(id);
            return View(m);
        }
        [HttpPost]
        public ActionResult Replied(Message m)
        {
            try
            {
                Customer c = crrepo.GetById(m.Sender_Id);
                string email = c.Email;
                Message m1 = mrepo.GetById(m.Id.ToString());
                m1.Status = 1;

                MessageRepository mr = new MessageRepository(new FinalProjectEntities());
                mr.update(m1);

                SmtpClient clientDetais = new SmtpClient();
                clientDetais.Port = 587;
                clientDetais.Host = "smtp.gmail.com";
                clientDetais.EnableSsl = true;
                clientDetais.DeliveryMethod = SmtpDeliveryMethod.Network;
                clientDetais.UseDefaultCredentials = false;
                clientDetais.Credentials = new NetworkCredential("librarymanagementSystemAIUB@gmail.com", "khalidhasan.khr");

                MailMessage maildetails = new MailMessage();
                maildetails.From = new MailAddress("librarymanagementSystemAIUB@gmail.com");
                maildetails.To.Add(email);
                maildetails.Subject = "GOOD NEWS";
                maildetails.Body = m.Messages;
                clientDetais.Send(maildetails);
                return RedirectToAction("Message");
            }
            catch (Exception)
            {
                return RedirectToAction("Message");
            }
            
        }
    }
}