//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Finalterm.Models
{
    using Finalterm.Validation;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class frentite
    {
        public int Id { get; set; }
        [Required, BannerIdValid]
        public int productId { get; set; }
        public string time { get; set; }

        public virtual Catagory Catagory { get; set; }
    }
}
