﻿using Finalterm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Finalterm.Repository
{
    public class CartsRepository: Repository<Cart>
    {
        private FinalProjectEntities fr = new FinalProjectEntities();
        public CartsRepository(FinalProjectEntities fr) : base(fr)
        {

        }
        
        public List<Cart> GetByIdc(string id)
        {
            return fr.Carts.Where(x => x.C_ID.Contains(id)).ToList();
        }
        
    }
}